import React from 'react';
import Banner from './Banner';
import AboutMe from './AboutMe';
import Skills from './Skills';
import MyWork from './MyWork';
import Services from './Services';
import ContactMe from './ContactMe';
import Footer from './Footer';

export default class WebMain extends React.Component {
    render() {
        return (
            <div>
                <Banner />
                <AboutMe />
                <Skills />
                <MyWork />
                <Services />
                <ContactMe />
                <Footer />
            </div>
        );
    }
}