import React from 'react';

const ContactMe = () => (

    <div className="contact-w3-agileits w3ls-section" id="contact">
        <div className="container">
            <h3 className="agileits-title cont-w3l">Feel free to contact me</h3>
            <div className="contact-main-w3ls">
                <div className="col-md-6  contact-left-w3ls">
                    <div className="mail contact-grid-agileinfo">
                        <div className="col-md-4 col-sm-4 col-xs-4 contact-icon-wthree">
                            <h4>Mail me</h4>
                        </div>
                        <div className="col-md-8  col-sm-8 col-xs-8 contact-text-agileinfo">
                            <a href="mailto:info@example.com">info@example.com</a><br />
                            <a href="mailto:info@example.com">w3ls@example.com</a>
                        </div>
                        <div className="clearfix" />
                    </div>
                    <div className="call contact-grid-agileinfo">
                        <div className="col-md-4 col-sm-4 col-xs-4 contact-icon-wthree">
                            <h4>Phone</h4>
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-8 contact-text-agileinfo">
                            <p>+18044261149</p>
                            <p>+18045261149</p>
                        </div>
                        <div className="clearfix" />
                    </div>
                    <div className="contact-grid-agileinfo">
                        <div className="col-md-4 col-sm-4 col-xs-4 contact-icon-wthree">
                            <h4>Address</h4>
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-8 contact-text-agileinfo">
                            <p>1234 Somewhere Road #5432<br /> Nashville, TN 00000<br /> USA.
            </p>
                        </div>
                        <div className="clearfix" />
                    </div>
                    <div className="visit wthree-social-icons contact-grid-agileinfo">
                        <div className="col-md-4 col-sm-4 col-xs-4 contact-icon-wthree">
                            <h4>Follow me</h4>
                        </div>
                        <div className="col-md-8 col-sm-8 col-xs-8 contact-text-agileinfo">
                            <ul className="w3-links">
                                <li><a href="#"><i className="fa fa-facebook" /></a></li>
                                <li><a href="#"><i className="fa fa-twitter" /></a></li>
                                <li><a href="#"><i className="fa fa-linkedin" /></a></li>
                                <li><a href="#"><i className="fa fa-google-plus" /></a></li>
                            </ul>
                        </div>
                        <div className="clearfix" />
                    </div>
                </div>
                <div className="contact-main">
                    <div className="col-md-6 agileits-main-right">
                        <form action="#" method="post" className="agile_form">
                            <label className="header">Name</label>
                            <div className="icon1 w3ls-name1">
                                <input placeholder=" " name="first name" type="text" required />
                            </div>
                            <div className="icon2">
                                <label className="header">Email</label>
                                <input placeholder=" " name="Email" type="email" required />
                            </div>
                            <label className="header">your message</label>
                            <textarea className="w3l_summary" required defaultValue={""} />
                            <input type="submit" defaultValue="SEND" />
                        </form>
                    </div>
                    <div className="clearfix" />
                </div>
            </div>
        </div>
    </div>

);

export default ContactMe;