import React from 'react';

const MyWork = () => (
    <div className="w3ls-section agileits-gallery" id="gallery">
        <div className="container">
            <h3 className="agileits-title">MY work</h3>
            <div className="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <div id="myTabContent" className="tab-content">
                    <div role="tabpanel" className="tab-pane fade in active" id="home-main" aria-labelledby="home-tab">
                        <div className="w3_tab_img">
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g1.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2">
                                            <img src="images/g1.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Consectetur</p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g2.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2">
                                            <img src="images/g2.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Adipiscing </p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g3.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g3.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Ullamcorper </p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g4.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g4.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Tristique</p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g5.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g5.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>accumsan</p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g6.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g6.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Vulputate</p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g7.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g7.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Sodales</p>
                                </div>
                            </div>
                            <div className="col-sm-3 w3_tab_img_left">
                                <div className="demo">
                                    <a className="cm-overlay" href="images/g8.jpg">
                                        <figure className="imghvr-shutter-in-out-diag-2"><img src="images/g8.jpg" alt=" " className="img-responsive" />
                                        </figure>
                                    </a>
                                </div>
                                <div className="agile-gallery-info">
                                    <h5>My project</h5>
                                    <p>Ornare </p>
                                </div>
                            </div>
                            <div className="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

);

export default MyWork;