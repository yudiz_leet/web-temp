import React from 'react';

const AboutMe = () => (

    <div>
        <div className="agile-about w3ls-section text-center" id="about">
            <div className="container">
                <h3 className="agileits-title">about me</h3>
                <div className="agileits-about-grid">
                    <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, sunt in culpa qui officia
                      deserunt mollit anim id est laboth. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
          fugiat nulla pariatur sunt in culpa qui .</p>
                </div>
                <a href="#contact" className="wthree- about-link scroll">hire me</a>
            </div>
        </div>
        <div className="agileits-about-btm">
            <div className="container">
                <div className="w3-flex">
                    <div className="col-md-4 col-sm-4 col-xs-12 ab1 agileits-about-grid1">
                        <span className="fa fa-trophy" aria-hidden="true" />
                        <h4 className="agileinfo-head">awards</h4>
                        <h5>best website design</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                        <h5>site of the day</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                        <h5>premier UX award</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                    </div>
                    <div className="col-md-4 col-sm-4 ab1 agileits-about-grid2">
                        <span className="fa fa-graduation-cap  wthree-title-list" aria-hidden="true" />
                        <h4 className="agileinfo-head">education</h4>
                        <h5>Phd Computer Science</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                        <h5>University</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                        <h5>High School</h5>
                        <p>Ncididunt ut labore et t enim ad minim.</p>
                    </div>
                    <div className="col-md-4 col-sm-4 ab1 agileits-about-grid3">
                        <span className="fa fa-shield  wthree-title-list" aria-hidden="true" />
                        <h4 className="agileinfo-head">expertise</h4>
                        <h5>Senior Interface Designer</h5>
                        <p>20XX-20XX</p>
                        <h5>Graphic Designer</h5>
                        <p>20XX-20XX</p>
                        <h5>Web Designer</h5>
                        <p>20XX-20XX</p>
                    </div>
                    <div className="clearfix" />
                </div>
            </div>
        </div>
    </div>
);

export default AboutMe;