import React from 'react';

const Skills = () => (
    <div className="agile-service w3ls-section" id="skills">
        <div className="container">
            <div className="col-md-5 w3_agileits-service-left">
                <h3 className="agileits-title">skills</h3>
                <p>Ncididunt ut labore et t enim ad minim.cididunt ut labore et t enim ad minim.Ncididunt ut labore et t enim ad minim labore
        et t enim ad minim.Ncididunt ut labore et t enim a.</p>
            </div>
            <div className="col-md-7 skills-right">
                <div className="vertical-skills  pull-right xs-center">
                    <ul className="list-inline">
                        <li>
                            <div className="skill" style={{ height: '100%', background: '#ff9d0d' }}><span className="value">100%</span></div><span className="title">HTML</span></li>
                        <li>
                            <div className="skill" style={{ height: '90%', background: '#03a9f4' }}><span className="value">90%</span></div><span className="title">CSS</span></li>
                        <li>
                            <div className="skill" style={{ height: '80%', background: '#b32eca' }}><span className="value">80%</span></div><span className="title">PHP</span></li>
                        <li>
                            <div className="skill" style={{ height: '75%', background: '#009688' }}><span className="value">75%</span></div><span className="title">ASP</span></li>
                        <li>
                            <div className="skill" style={{ height: '85%', background: '#6361f0' }}><span className="value">85%</span></div><span className="title">JS</span></li>
                    </ul>
                </div>
            </div>
            <div className="clearfix" />
        </div>
    </div>
);

export default Skills;