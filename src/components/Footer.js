import React from 'react';

const Footer = () => (
    <div className="agileits_w3layouts-footer">
        <p>© 2017 Designer. All rights reserved | Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
    </div>
);

export default Footer;