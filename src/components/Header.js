import React from 'react';

const Header = () => (
    // header
    <div className="header">
        <div className="container">
            <div className="menu">
                <a href id="menuToggle"> <span className="navClosed" /> </a>
                <nav>
                    <a href="#home" className="active scroll">Home</a>
                    <a href="#about" className="scroll">About Me</a>
                    <a href="#skills" className="scroll">Skills</a>
                    <a href="#gallery" className="scroll">My Work</a>
                    <a href="#services" className="scroll">Services</a>
                    <a href="#contact" className="scroll">Contact</a>
                </nav>
            </div>
            <div className="clearfix"> </div>
        </div>
    </div>

    // header end
);

export default Header;