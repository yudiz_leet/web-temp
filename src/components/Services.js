import React from 'react';

const Services = () => (

    <div className="w3ls-section services" id="services">
        <div className="container">
            <h3 className="agileits-title text-center">services</h3>
            <div className="services-w3ls-row agileits-w3layouts service1 text-center">
                <h5>web design</h5>
                <p>Tenetur a sapiente itaque earum rerum hic delectus maiores alias phasellus.</p>
                <div className="s-top">
                    <span className="fa fa-laptop" aria-hidden="true" />
                </div>
            </div>
            <div className="services-w3ls-row agileits-w3layouts middlegrid-w3ls">
                <div className="col-md-4 services-grid agileits-w3layouts service2">
                    <div className="col-md-10 w3ls-sub-text">
                        <h5>ui design</h5>
                        <p>Tenetur a sapiente itaque earum rerum hic delectus maiores alias phasellus.</p>
                    </div>
                    <div className="col-md-2 sub-icon">
                        <span className="fa fa-caret-square-o-up" aria-hidden="true" />
                    </div>
                </div>
                <div className="col-md-4 services-grid img-agileits">
                    <img src="images/services.jpg" className="img-responsive" alt />
                </div>
                <div className="col-md-4 services-grid agileits-w3layouts service3">
                    <div className="col-md-2 sub-icon">
                        <span className="fa fa-grav" aria-hidden="true" />
                    </div>
                    <div className="col-md-10 w3ls-sub-text">
                        <h5>graphic design</h5>
                        <p>Tenetur a sapiente itaque earum rerum hic delectus maiores alias phasellus.</p>
                    </div>
                </div>
                <div className="clearfix"> </div>
            </div>
            <div className="services-w3ls-row agileits-w3layouts service4">
                <div className="s-top">
                    <span className="fa fa-adjust" aria-hidden="true" />
                </div>
                <h5>seo</h5>
                <p>Tenetur a sapiente itaque earum rerum hic delectus maiores alias phasellus.</p>
            </div>
        </div>
    </div>


);

export default Services;