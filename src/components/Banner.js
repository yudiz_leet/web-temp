import React from 'react';
import Header from './Header';

const Banner = () => (
    // banner
    <div id="home" className="banner">
        <div className="banner-agileinfo">
            <Header />
            <div className="banner-text">
                <div className="container">
                    <h1><a href="index.html">Mathew</a></h1>
                    <h2 className="w3ls-bnrtext">I am <span> UX </span>&amp; <span>UI </span> Designer</h2>
                    <p className="w3ls-p">an interactive web designer with the passion for creativity</p>
                    <a href="#about" className="buy btn-wayra scroll">Explore my work</a>
                </div>
            </div>
        </div>
    </div>
    // banner end
);

export default Banner;