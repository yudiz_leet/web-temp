import React from 'react';
import ReactDOM from 'react-dom';
import WebMain from './components/WebMain';

ReactDOM.render(<WebMain />, document.getElementById('app'));